﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameSave.Common.Misc
{
    public class DateTimeUtil
    {
        public static string CalcTimeSpan(DateTime dt)
        {
            if(dt > DateTime.Now)
            {
                return $"错误的时间:'{dt.ToString("yyyy-MM-dd HH:mm:ss")}'";
            }
            else
            {
                TimeSpan ts = DateTime.Now - dt;
                if (ts.TotalSeconds < 60)
                {
                    return "1分钟之前";
                }
                else if (ts.TotalMinutes < 60)
                {
                    return $"{ts.TotalMinutes.ToString()}分钟之前";
                }
                else if (ts.TotalHours > 1 && ts.TotalHours <= 24)
                {
                    return $"{ts.TotalHours}小时之前";
                }
                else if (ts.TotalDays < 30)
                {
                    return $"{ts.TotalDays}天之前";
                }
                else if (ts.TotalDays >= 30 && ts.TotalDays < 365)
                {
                    int mon = (int)ts.TotalDays / 30;
                    return $"{mon}个月之前";
                }
                else if (ts.TotalDays > 365)
                {
                    int year = (int)ts.TotalDays / 365;
                    int mon = ((int)ts.TotalDays % 365) / 30;
                    return $"{year}年又{mon}个月之前";
                }
                else
                {
                    return "无法计算时差";
                }
            }
            
        }
    }
}

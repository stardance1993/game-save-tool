﻿using GameSave.Infrastructure.Entities;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameSave.Common.Misc
{
    public class HotKeyHelper
    {
        /// 
        /// 记录快捷键注册项的唯一标识符
        /// 
        private static Dictionary<EHotKeyAction, int> m_HotKeySettingsDic = new Dictionary<EHotKeyAction, int>();


        /// 
        /// 注册系统快捷键
        /// 
        /// 待注册快捷键项
        /// 窗口句柄
        /// 快捷键注册项的唯一标识符字典
        /// 返回注册失败项的拼接字符串
        public static string RegisterGlobalHotKey(List<HotKeyModel> hotKeyModelList, IntPtr hwnd, out Dictionary<EHotKeyAction, int> EHotKeyActionsDic)
        {
            string FailRegKeyDesc = string.Empty;
            foreach (var item in hotKeyModelList)
            {
                if (!RegisterHotKey(item, hwnd))
                {
                    string combineKeyDesc = string.Empty;
                    if (item.IsControl && !item.IsShift && !item.IsAlt)
                    {
                        combineKeyDesc = System.Windows.Input.ModifierKeys.Control.ToString();
                    }
                    else if (!item.IsControl && item.IsShift && !item.IsAlt)
                    {
                        combineKeyDesc = System.Windows.Input.ModifierKeys.Shift.ToString();
                    }
                    else if (!item.IsControl && !item.IsShift && item.IsAlt)
                    {
                        combineKeyDesc = System.Windows.Input.ModifierKeys.Alt.ToString();
                    }
                    else if (item.IsControl && item.IsShift && !item.IsAlt)
                    {
                        combineKeyDesc = string.Format("{0}+{1}", System.Windows.Input.ModifierKeys.Control.ToString(), System.Windows.Input.ModifierKeys.Shift);
                    }
                    else if (item.IsControl && !item.IsShift && item.IsAlt)
                    {
                        combineKeyDesc = string.Format("{0}+{1}", System.Windows.Input.ModifierKeys.Control.ToString(), System.Windows.Input.ModifierKeys.Alt);
                    }
                    else if (!item.IsControl && item.IsShift && item.IsAlt)
                    {
                        combineKeyDesc = string.Format("{0}+{1}", System.Windows.Input.ModifierKeys.Shift.ToString(), System.Windows.Input.ModifierKeys.Alt);
                    }
                    else if (item.IsControl && item.IsShift && item.IsAlt)
                    {
                        combineKeyDesc = string.Format("{0}+{1}+{2}", System.Windows.Input.ModifierKeys.Control.ToString(), System.Windows.Input.ModifierKeys.Shift.ToString(), System.Windows.Input.ModifierKeys.Alt);
                    }
                    combineKeyDesc += $"+{item.Key.ToString()}";
                    combineKeyDesc = $"{combineKeyDesc}[{item.ActionName}]";
                    FailRegKeyDesc += $"{combineKeyDesc};";
                }
            }
            EHotKeyActionsDic = m_HotKeySettingsDic;
            return FailRegKeyDesc;
        }


        /// 
        /// 注册热键
        /// 
        /// 热键待注册项
        /// 窗口句柄
        /// 成功返回true，失败返回false
        private static bool RegisterHotKey(HotKeyModel hotKeyModel, IntPtr hWnd)
        {
            var fsModifierKey = new System.Windows.Input.ModifierKeys();
            EHotKeyAction hotKeyAction = (EHotKeyAction)Enum.Parse(typeof(EHotKeyAction), hotKeyModel.ActionName);

            if (!m_HotKeySettingsDic.ContainsKey(hotKeyAction))
            {
                // 全局原子不会在应用程序终止时自动删除。每次调用GlobalAddAtom函数，必须相应的调用GlobalDeleteAtom函数删除原子。
                if (HotKeyManager.GlobalFindAtom(hotKeyAction.ToString()) != 0)
                {
                    HotKeyManager.GlobalDeleteAtom(HotKeyManager.GlobalFindAtom(hotKeyAction.ToString()));
                }
                // 获取唯一标识符
                m_HotKeySettingsDic[hotKeyAction] = HotKeyManager.GlobalAddAtom(hotKeyAction.ToString());
            }
            else
            {
                // 注销旧的热键
                HotKeyManager.UnregisterHotKey(hWnd, m_HotKeySettingsDic[hotKeyAction]);
            }

            // 注册热键
            if (hotKeyModel.IsControl && !hotKeyModel.IsShift && !hotKeyModel.IsAlt)
            {
                fsModifierKey = System.Windows.Input.ModifierKeys.Control;
            }
            else if (!hotKeyModel.IsControl && hotKeyModel.IsShift && !hotKeyModel.IsAlt)
            {
                fsModifierKey = System.Windows.Input.ModifierKeys.Shift;
            }
            else if (!hotKeyModel.IsControl && !hotKeyModel.IsShift && hotKeyModel.IsAlt)
            {
                fsModifierKey = System.Windows.Input.ModifierKeys.Alt;
            }
            else if (hotKeyModel.IsControl && hotKeyModel.IsShift && !hotKeyModel.IsAlt)
            {
                fsModifierKey = System.Windows.Input.ModifierKeys.Control | System.Windows.Input.ModifierKeys.Shift;
            }
            else if (hotKeyModel.IsControl && !hotKeyModel.IsShift && hotKeyModel.IsAlt)
            {
                fsModifierKey = System.Windows.Input.ModifierKeys.Control | System.Windows.Input.ModifierKeys.Alt;
            }
            else if (!hotKeyModel.IsControl && hotKeyModel.IsShift && hotKeyModel.IsAlt)
            {
                fsModifierKey = System.Windows.Input.ModifierKeys.Shift | System.Windows.Input.ModifierKeys.Alt;
            }
            else if (hotKeyModel.IsControl && hotKeyModel.IsShift && hotKeyModel.IsAlt)
            {
                fsModifierKey = System.Windows.Input.ModifierKeys.Control | System.Windows.Input.ModifierKeys.Shift | System.Windows.Input.ModifierKeys.Alt;
            }
            return HotKeyManager.RegisterHotKey(hWnd, m_HotKeySettingsDic[hotKeyAction], fsModifierKey, (int)hotKeyModel.Key);
        }
    }
}

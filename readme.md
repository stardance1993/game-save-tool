## 游戏存档小助手

Game.Archive.BackupTool

一个辅助游戏玩家，管理游戏存档的小工具

可以按游戏创建存档任务

可以按照单个文件，整个文件夹自动触发备份；进行定时存档，或者手动存档

支持全局快捷键操作

本软件基于.Net Framework 4.6.2 框架；如果启动报错，请先安装[运行环境](https://dotnet.microsoft.com/zh-cn/download/dotnet-framework/thank-you/net462-offline-installer)

---

**软件截图**

![](https://gitee.com/stardance1993/game-save-tool/raw/master/Resources/mainWindow.jpg "主页面")

---

## 软件使用

### 1.设置

1. 启动软件，点击菜单‘通用’-‘设置’
   
   ![](https://gitee.com/stardance1993/game-save-tool/raw/master/Resources/setup-01.jpg "设置界面")

2. 设置存档文件保存的文件夹，自动备份间隔冷却时间(建议设置为30秒以上)

3. 重启软件，在主界面点击‘新增’按钮，打开创建任务对话框
   ![](https://gitee.com/stardance1993/game-save-tool/raw/master/Resources/createJob.jpg "创建任务界面")

4. 填写任务名称，比如‘黑暗之魂3’

5. 选择任务的类型，这里的类型指的是：
+ 单文件备份:当游戏存档只有一个文件时，自动触发备份

+ 多文件备份:当存档可能时多个不同文件时，任意一个文件被修改，自动触发备份

+ 定时备份:不是按照存档文件发生修改时触发备份，而是每隔一个时间自动进行备份

+ 手动备份:不会进行任何自动备份，完全依赖玩家手动点击来创建备份
5. 将游戏的存档文件(或文件夹)填入文本框‘目标文件’或‘目标文件夹’
   
   > 游戏的存档位置，请自行百度，鄙人因时间有限，没有提供自动查询游戏存档路径的功能

6. 如果任务类型选择**定时备份**，需要在下拉框‘自动备份频率’选择备份间隔，表示每隔x分钟就产生一个存档

7. 选择自动存档上限，自动产生的存档会无限增加，所以过时的自动存档必须清理，以免列表中有太多的存档产生混乱   

8. 点击上方‘快捷键’，可以设置手动存档和手动载入存档的全局快捷键，手动存档可以在任何时候进行；手动载入存档之前，必须先切换到本程序，选中指定的备份存档，下方的文本框显示‘选中存档:xxx’的时候，才能在游戏内覆盖存档

9. 编辑完成后，点击‘**确认**’保存
   
   ### 2.使用

10. 重启软件，如果左侧备份任务带有启动，停止按钮，点击按钮来启动(暂停)任务,当游戏存档变化时，软件会侦测到变化并进行存档备份(有30秒以上的延迟)，自动在右侧存档列表产生新的备份记录;如果是手动的任务，点击‘**手动备份**’来备份游戏存档
    ![](https://gitee.com/stardance1993/game-save-tool/raw/master/Resources/jobList.jpg "备份任务列表")

11. 载入存档。首![](https://gitee.com/stardance1993/game-save-tool/raw/master/Resources/backupList.jpg "载入存档")先将游戏返回到主界面(如果此时游戏触发自动存档，等待几秒钟)，然后切换到本程序，选择你想要回退的存档记录，点击‘**载入**’按钮，看到提示成功后，在游戏内选择‘继续游戏’，游戏的状态就会回到指定存档的时候。

12. 存档右侧有几个按钮，功能分别是:
1) **重命名**，重新命名存档

2) **收藏**，标记一个存档为收藏，收藏的存档高亮显示，并且不会被自动删除

3) **在文件夹中浏览**，在资源管理器中查看存档备份

4) **载入**，将备份的存档复制到游戏存档目录，覆盖存档文件

---

## 鸣谢

感谢以下开源项目:

+ [QuickFramework.Wpf](https://github.com/autcn/Quick.Framework)

+ [AutoMapper](https://automapper.org/)

+ [Panuon.WPF](https://github.com/Panuon/Panuon.WPF)

+ [Fody](https://github.com/Fody/Fody)

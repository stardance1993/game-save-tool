﻿using GameSave.Infrastructure.Message;
using Quick;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace GameSave.UI.UC
{
    /// <summary>
    /// SysCfgWindow.xaml 的交互逻辑
    /// </summary>
    public partial class SysCfgWindow : Window
    {
        private SysCfgWindowVM _viewModel;
        

        public SysCfgWindow(SysCfgWindowVM dataContext)
        {
            InitializeComponent();
            _viewModel = dataContext;
            _viewModel.OnCloseWinRequested += () =>
            {
                this.Close();
            };
            this.DataContext = _viewModel;

        }

        //private void tbSelfBackupKeyDown_KeyDown(object sender, KeyEventArgs e)
        //{
        //    string cmd = GetCommand(e);
        //    Messenger.Default.Send<ShortcutTypeinMessage>(new ShortcutTypeinMessage
        //    {
        //        KeyFunction = "ManualBackup",
        //        KeyCombine = cmd
        //    });
        //    e.Handled = true;
        //}

        //private void tbSelfLoadBackup_KeyDown(object sender, KeyEventArgs e)
        //{
        //    string cmd = GetCommand(e);
        //    Messenger.Default.Send<ShortcutTypeinMessage>(new ShortcutTypeinMessage
        //    {
        //        KeyFunction = "LoadBackup",
        //        KeyCombine = cmd
        //    });
        //    e.Handled = true;
        //}

        private string GetCommand(KeyEventArgs e)
        {
            string command = string.Empty;
            if (e.KeyboardDevice.Modifiers.HasFlag(ModifierKeys.Control))
            {
                command += $"Control";
            }

            if (e.KeyboardDevice.Modifiers.HasFlag(ModifierKeys.Shift))
            {
                if (!string.IsNullOrEmpty(command))
                {
                    command += $"+Shift";
                }
                else
                {
                    command += $"Shift";
                }
            }

            if (e.KeyboardDevice.Modifiers.HasFlag(ModifierKeys.Alt))
            {
                if (!string.IsNullOrEmpty(command))
                {
                    command += $"+Alt";
                }
                else
                {
                    command += $"Alt";
                }
            }

            if (!string.IsNullOrEmpty(command))
            {
                command += $"+{e.Key.ToString()}";
            }
            else
            {
                command += $"{e.Key.ToString()}";
            }

            return command;
        }
    }
}

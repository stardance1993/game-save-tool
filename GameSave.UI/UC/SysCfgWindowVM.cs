﻿using GameSave.Common.Misc;
using GameSave.Infrastructure.Data;
using GameSave.Infrastructure.Entities;
using GameSave.Infrastructure.Message;
using Quick;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GameSave.UI.UC
{
    public class SysCfgWindowVM:QBindableBase
    {
        private SysConfig _sysCfg;

        public SysConfig SysCfg
        {
            get { return _sysCfg; }
            set { _sysCfg = value; }
        }


        private HotKeyModel _selfBackupModel;

        public HotKeyModel SelfHelpBackUpKeyModel
        {
            get { return _selfBackupModel; }
            set { _selfBackupModel = value; }
        }

        private HotKeyModel _loadBackupModel;

        public HotKeyModel LoadBackUpKeyModel
        {
            get { return _loadBackupModel; }
            set { _loadBackupModel = value; }
        }



        private ObservableCollection<int> _coldDownOptions;

        /// <summary>
        /// 短时间内产生了多次存档修改,
        /// 取最后一次修改的存档为有效修改,
        /// 这个选项控制间隔多久的存档为有效
        /// </summary>
        public ObservableCollection<int> TriggerBackupColdDownOptions
        {
            get { return _coldDownOptions; }
            set { _coldDownOptions = value; }
        }


        private ObservableCollection<string> _allKeyChars;

        public ObservableCollection<string> AllKeyChars
        {
            get { return _allKeyChars; }
            set { _allKeyChars = value; }
        }



        private int _selectedColdDownOptoion;

        public int SelectedColdDownOption
        {
            get { return _selectedColdDownOptoion; }
            set { _selectedColdDownOptoion = value; }
        }

        private ObservableCollection<string> _languages;

        public ObservableCollection<string> Languages
        {
            get { return _languages; }
            set { _languages = value; }
        }



        //private string _selectedLanguage;
        //public string SelectedLanguage
        //{
        //    get { return _selectedLanguage; }
        //    set 
        //    {
        //        if (!string.IsNullOrEmpty(value))
        //        {
        //            _selectedLanguage = value;
        //            SetLanguage(_selectedLanguage); 
        //        }
        //    }
        //}



        public Action OnCloseWinRequested { get; set; }


        #region private prop
        private IMessageBox _msgBox;
        private static readonly string ConfigPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, DefaultConfig.ConfigDirectory);
        private static readonly string jobConfigFileName = Path.Combine(ConfigPath, DefaultConfig.JobConfigFileName);
        private static readonly string sysConfigFileName = Path.Combine(ConfigPath, DefaultConfig.SysConfigFileName);
        private string sysConfigFileFullPath;
        #endregion
        public SysCfgWindowVM(IMessageBox msgB)
        {
            sysConfigFileFullPath = Path.Combine(ConfigPath, sysConfigFileName);
            if(File.Exists(sysConfigFileFullPath))
            {
                SysCfg = ConfigHelper.GetConfig<SysConfig>(sysConfigFileFullPath);
            }
            else
            {
                SysCfg = new SysConfig();
            }

            TriggerBackupColdDownOptions = new ObservableCollection<int> { 30, 60, 120, 150, 180, 240 };

            List<string> keyList =  Enum.GetNames(typeof(EKey)).ToList();
            if(keyList.Contains("Shift"))
            {
                keyList.Remove("Shift");
            }
            if (keyList.Contains("Control"))
            {
                keyList.Remove("Control");

            }
            if (keyList.Contains("Alt"))
            {
                keyList.Remove("Alt");
            }
            AllKeyChars = new ObservableCollection<string>(keyList);

            SelfHelpBackUpKeyModel = GetKeyModel(SysCfg.SelfHelpBackupShortcut);
            LoadBackUpKeyModel = GetKeyModel(SysCfg.LoadBackupShortcut);

            Languages = new ObservableCollection<string>(Enum.GetNames(typeof(Language)).ToList());

            _msgBox = msgB;

            //Languages
        }

        public void OnLanguageChange()
        {
            string language = SysCfg.SelectedLanguage;
            if(!string.IsNullOrEmpty(language)) 
            {
                
            }
            
        }

        public void ConfirmModification()
        {
            if(string.IsNullOrEmpty(SysCfg.SaveDirectory))
            {
                _msgBox.Show("存档备份主目录不可为空!");
                return;
            }

            if(!Directory.Exists(SysCfg.SaveDirectory)) 
            {
                _msgBox.Show("存档备份主目录不存在!\n请重新设定");
                return;
            }

            //读取快捷键设定
            string selfBackupShortcut = string.Empty;
            selfBackupShortcut = GetShortcutCmd(SelfHelpBackUpKeyModel);
            string loadBackupShortcut = string.Empty;
            loadBackupShortcut = GetShortcutCmd(LoadBackUpKeyModel);
            SysCfg.SelfHelpBackupShortcut = selfBackupShortcut;
            SysCfg.LoadBackupShortcut = loadBackupShortcut;

            if(!string.IsNullOrEmpty(SysCfg.SelfHelpBackupShortcut) &&
               !string.IsNullOrEmpty(SysCfg.LoadBackupShortcut) &&
               SysCfg.SelfHelpBackupShortcut.Equals(SysCfg.LoadBackupShortcut))
            {

            }

            ConfigHelper.SetConfig<SysConfig>(SysCfg, sysConfigFileFullPath);
            Messenger.Default.Send<NotifyMessage>(new NotifyMessage
            {
                Content = "设置已保存."
            });
            OnCloseWinRequested.Invoke();
        }

        public HotKeyModel GetKeyModel(string shortcutCmd)
        {
            HotKeyModel model = new HotKeyModel();
            if (!string.IsNullOrEmpty(shortcutCmd))
            {
                if (shortcutCmd.Contains("Control"))
                {
                    model.IsControl = true;

                }
                if (shortcutCmd.Contains("Shift"))
                {
                    model.IsShift = true;

                }
                if (shortcutCmd.Contains("Alt"))
                {
                    model.IsAlt = true;
                }

                string specialChar = shortcutCmd.Replace("Control", "").Replace("Shift", "").Replace("Alt", "").Replace("+", "");
                if(!string.IsNullOrEmpty(specialChar))
                {
                    model.KeyName = specialChar;
                    model.Key = (EKey)Enum.Parse(typeof(EKey), specialChar);
                }
            }
            return model;
        }


        public string GetShortcutCmd(HotKeyModel model)
        {
            string shortcutCmd = string.Empty;
            if (model.IsControl)
            {
                shortcutCmd += $"Control";
            }
            if (model.IsShift)
            {
                if (string.IsNullOrEmpty(shortcutCmd))
                {
                    shortcutCmd += $"Shift";
                }
                else
                {
                    shortcutCmd += $"+Shift";

                }
            }
            if (model.IsAlt)
            {
                if (string.IsNullOrEmpty(shortcutCmd))
                {
                    shortcutCmd += $"Alt";
                }
                else
                {
                    shortcutCmd += $"+Alt";

                }
            }
            if (!string.IsNullOrEmpty(model.KeyName))
            {
                if (string.IsNullOrEmpty(shortcutCmd))
                {
                    shortcutCmd += $"{model.KeyName}";
                }
                else
                {
                    shortcutCmd += $"+{model.KeyName}";
                }
                SysCfg.SelfHelpBackupShortcut = shortcutCmd;
            }
            else
            {
                //不选择英文按键，视为无效的快捷键
                shortcutCmd = string.Empty;
            }

            return shortcutCmd;
        }
    }
}

﻿using GameSave.Common.Entities;
using GameSave.Common.Misc;
using GameSave.Infrastructure.Entities;
using GameSave.Infrastructure.Message;
using Quick;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Navigation;

namespace GameSave.UI.UC
{
    public  class JobEditWindowVM:QBindableBase
    {
        private Job _curJob;

        public Job CurrentJob
        {
            get { return _curJob; }
            set { _curJob = value; NotifyPropertyChanged(nameof(CurrentJob)); JobTypeChangeCmd.Execute(null); }
        }



        private ObservableCollection<string> _jobTypes;

        public ObservableCollection<string> JobTypes
        {
            get { return _jobTypes; }
            set { _jobTypes = value; }
        }

        private ObservableCollection<int> _saveOpts;

        public ObservableCollection<int> SaveLimitOptions
        {
            get { return _saveOpts; }
            set { _saveOpts = value; }
        }

        private ObservableCollection<int> _autoSaveFreqs;

        public ObservableCollection<int> AutoBackupFrequencies
        {
            get { return _autoSaveFreqs; }
            set { _autoSaveFreqs = value; }
        }

        private string _selectedJobType;

        public string SelectedJobType
        {
            get { return _selectedJobType; }
            set { _selectedJobType = value; }
        }

        public bool DialogResult { get; set; }

        public Visibility OnOneFileOptionVisible { get; set; }

        public Visibility OnDirChangeOptionVisible { get; set; }

        public Visibility OnTimeTriggerOptionVisible { get; set; }

        public Visibility AutoSaveLimitOptionVisible { get; set; }




        private IMessageBox _msgBox;
        private string _configBaseDir;
        private string _backupBaseDir;

        public JobEditWindowVM(IMessageBox msgBox)
        {
            Array arrs = Enum.GetNames(typeof(JoBType));
            JobTypes = new ObservableCollection<string>();
            AutoBackupFrequencies = new ObservableCollection<int>();
            foreach (string s in arrs)
            {
                JobTypes.Add(s);
            };

            SaveLimitOptions = new ObservableCollection<int> {2, 4, 6, 8, 10, 15, 20 };
            AutoBackupFrequencies = new ObservableCollection<int> { 1, 2, 3, 4, 5, 10, 15 };
            CurrentJob = new Job();
            OnOneFileOptionVisible = Visibility.Collapsed;
            OnDirChangeOptionVisible = Visibility.Collapsed;
            OnTimeTriggerOptionVisible = Visibility.Collapsed;
            AutoSaveLimitOptionVisible = Visibility.Collapsed;
            _msgBox = msgBox;
        }

        public void SetConfig(string configBaseDir, string backupBaseDir)
        {
            this._configBaseDir = configBaseDir;
            this._backupBaseDir = backupBaseDir;
        }

        public void ResetJobCmd()
        {
            var result = _msgBox.QuestionYesNo("重置任务将删除当前游戏的全部备份存档\n是否继续?");
            if (result == MessageBoxResult.Yes)
            {
                //清空存档
                string backupDir = Path.Combine(_backupBaseDir, CurrentJob.JobId);
                if (Directory.Exists(backupDir))
                {
                    Directory.Delete(backupDir, true);
                }
                //重置序号
                CurrentJob.SaveOperationCount = 0;

                //保存到cfg,主页面重载job列表

                Messenger.Default.Send<ReloadJobMessage>(new ReloadJobMessage
                {
                    PrepareUpdateJob = CurrentJob
                });
            }
        }

        private ICommand _JobTypeChangeCmd;

        public ICommand JobTypeChangeCmd
        {
            get
            {
                if(_JobTypeChangeCmd == null)
                {
                    _JobTypeChangeCmd = new RelayCommand((p) =>
                    {
                        if (CurrentJob != null && CurrentJob.JobType != null)
                        {
                            if (CurrentJob.JobType == JoBType.单文件备份.ToString())
                            {
                                OnOneFileOptionVisible = Visibility.Visible;
                                OnDirChangeOptionVisible = Visibility.Collapsed;
                                OnTimeTriggerOptionVisible = Visibility.Collapsed;
                                AutoSaveLimitOptionVisible = Visibility.Visible;

                            }
                            else if (CurrentJob.JobType == JoBType.多文件备份.ToString())
                            {
                                OnOneFileOptionVisible = Visibility.Collapsed;
                                OnDirChangeOptionVisible = Visibility.Visible;
                                OnTimeTriggerOptionVisible = Visibility.Collapsed;
                                AutoSaveLimitOptionVisible = Visibility.Visible;

                            }
                            else if (CurrentJob.JobType == JoBType.定时备份.ToString())
                            {
                                OnOneFileOptionVisible = Visibility.Visible;
                                OnDirChangeOptionVisible = Visibility.Visible;
                                OnTimeTriggerOptionVisible = Visibility.Visible;
                                AutoSaveLimitOptionVisible = Visibility.Visible;

                            }
                            else
                            {
                                OnOneFileOptionVisible = Visibility.Visible;
                                OnDirChangeOptionVisible = Visibility.Visible;
                                OnTimeTriggerOptionVisible = Visibility.Collapsed;
                                AutoSaveLimitOptionVisible = Visibility.Collapsed;
                            }
                        }
                    });
                }
                return _JobTypeChangeCmd;
            }
        }


        private ICommand _resetJobCmd;

    }
}

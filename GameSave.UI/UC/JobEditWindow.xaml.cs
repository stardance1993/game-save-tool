﻿using GameSave.Common.Misc;
using GameSave.Infrastructure.Entities;
using Quick;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using static System.Windows.Forms.VisualStyles.VisualStyleElement;
using Panuon.WPF.UI;

namespace GameSave.UI.UC
{
    /// <summary>
    /// JobEditWindow.xaml 的交互逻辑
    /// </summary>
    public partial class JobEditWindow : System.Windows.Window
    {
        public JobEditWindowVM VM { private set; get; }
        private IMessageBox _msgBox;

        public JobEditWindow(JobEditWindowVM editWindowVM,IMessageBox MsgBox)
        {
            InitializeComponent();
            VM = editWindowVM;
            this.DataContext = VM;
            _msgBox = MsgBox;
        }

        private void ConfirmJobEdit(object sender, RoutedEventArgs e)
        {
            if (VM.CurrentJob.JobType != JoBType.手动备份.ToString())
            {
                //检查是否选择最大存档数
                if(VM.CurrentJob.AutoSaveBackupLimit == 0)
                {
                    _msgBox.Show("必须设定自动存档的上限。");
                    return;
                }
            }

            //检查是否输入了有效的备份文件(路径)
            if(string.IsNullOrEmpty(VM.CurrentJob.TargetBackupDirectory) &&
               string.IsNullOrEmpty(VM.CurrentJob.TargetBackupFile))
            {
                _msgBox.Show("备份文件或备份文件夹不能同时为空\n否则备份无法进行。");
                return;
            }

            //检查是否是合法文件夹或合法文件
            if(!string.IsNullOrEmpty(VM.CurrentJob.TargetBackupDirectory))
            {
                if(!Directory.Exists(VM.CurrentJob.TargetBackupDirectory))
                {
                    _msgBox.Show($"设置的备份文件夹不存在!\n{VM.CurrentJob.TargetBackupDirectory}");
                    return;
                }
            }

            if(!string.IsNullOrEmpty(VM.CurrentJob.TargetBackupFile))
            {
                if (!File.Exists(VM.CurrentJob.TargetBackupFile))
                {
                    _msgBox.Show($"设置的备份文件不存在!\n{VM.CurrentJob.TargetBackupFile}");
                    return;
                }
            }

            //Quick.Toast.Show("设置已保存", 1500);
            this.DialogResult = true;
        }

        private void CancelJobEdit(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }
    }
}

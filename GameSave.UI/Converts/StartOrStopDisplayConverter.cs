﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Media.Imaging;

namespace GameSave.UI.Converts
{
    public class StartOrStopDisplayConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            bool isRunning = (bool)value;
            string type = (string)parameter;
            if(type == "Start")
            {
                if(isRunning)
                {
                    string url = string.Format($"pack://application:,,,/GameSave.Common;component/Images/start_gray.png");
                    return new BitmapImage(new Uri(url));
                }
                else
                {
                    string url = string.Format($"pack://application:,,,/GameSave.Common;component/Images/start.png");
                    return new BitmapImage(new Uri(url));
                }
            }
            else if(type == "Stop")
            {
                //Stop Status
                if (isRunning)
                {
                    string url = string.Format($"pack://application:,,,/GameSave.Common;component/Images/stop.png");
                    return new BitmapImage(new Uri(url));
                }
                else
                {
                    string url = string.Format($"pack://application:,,,/GameSave.Common;component/Images/stop_gray.png");
                    return new BitmapImage(new Uri(url));
                }
            }
            else
            {
                return null;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}

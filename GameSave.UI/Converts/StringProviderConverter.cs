﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace GameSave.UI.Converts
{
    public class StringProviderConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            string paraExpression = (string)parameter;
            string[] paraArr = paraExpression.Split("|");
            if(paraArr[0].ToLower().Equals("bool"))
            {
                bool val = (bool)value;
                if(val)
                {
                    return paraArr[1];
                }
                else
                {
                    return paraArr[2];
                }
            }
            else
            {
                return "";
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}

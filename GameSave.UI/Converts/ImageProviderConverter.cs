﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Media.Imaging;

namespace GameSave.UI.Converts
{
    public class ImageProviderConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            
            //解析参数
            string[] paraFormat = parameter.ToString().Split('|');
            string valueType = paraFormat[0];
            string controlParaA = paraFormat[1];
            string controlParaB = paraFormat[2];

            if (valueType.ToLower().Equals("bool"))
            {
                bool bindProp = (bool)value;
                if (bindProp)
                {
                    string url = string.Format($"pack://application:,,,/GameSave.Common;component/Images/{controlParaA}.png");
                    return new BitmapImage(new Uri(url));
                }
                else
                {
                    string url = string.Format($"pack://application:,,,/GameSave.Common;component/Images/{controlParaB}.png");
                    return new BitmapImage(new Uri(url));
                }
            }
            else
            {
                return null;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}

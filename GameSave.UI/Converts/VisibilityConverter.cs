﻿using GameSave.Common.Misc;
using GameSave.Infrastructure.Entities;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;
using System.Windows.Media.Imaging;

namespace GameSave.UI.Converts
{
    public class VisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            string para = (string)parameter;
            string[] paraList = para.Split('|');
            string valueType = paraList[0];


            if (valueType.ToLower().Equals("bool"))
            {
                bool val = (bool)value;

                bool reverse = false;
                if (paraList.Length > 1)
                {
                    string rev = paraList[1];
                    if (rev.ToLower().Equals("reverse"))
                    {
                        val = !val;
                    }
                }

                if (val)
                {
                    return Visibility.Visible;
                }
                else
                {
                    return Visibility.Collapsed;
                }
            }
            else if (valueType.ToLower().Equals("jobtype"))
            {

                string val = (string)value;
                if (val.Equals(JoBType.手动备份.ToString()))
                {
                    return Visibility.Collapsed;
                }
                else
                {
                    return Visibility.Visible;
                }
            }
            else
            {
                return Visibility.Visible;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Media.Imaging;

namespace GameSave.UI.Converts
{
    /// <summary>
    /// 弃用，改为使用转换器‘ImageProviderConverter’
    /// </summary>
    public class LikeConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            bool isLike = (bool)value;
            if(isLike)
            {
                string url = string.Format($"pack://application:,,,/GameSave.Common;component/Images/like.png");
                return new BitmapImage(new Uri(url));
            }
            else
            {
                string url = $"pack://application:,,,/GameSave.Common;component/Images/dislike.png";
                return new BitmapImage(new Uri(url));
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}

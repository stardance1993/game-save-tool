﻿using Autofac;
using GameSave.Impl.JobExecutor;
using GameSave.UI.Converts;
using GameSave.UI.UC;
using Quick;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace GameSave.UI
{
    /// <summary>
    /// App.xaml 的交互逻辑
    /// </summary>
    public partial class App : QWpfApplication
    {
        public override Type StartupModuleType => typeof(AppModule);

        //为了从容器中获取窗口类，需要重写该方法来弹出窗口，需要在Xaml中将StartupUri="MainWindow.xaml"删除
        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);
            //从IOC容器中获取Mainwindow
            MainWindow mainWindow = ServiceProvider.GetService<MainWindow>();
            mainWindow.Show();
        }
    }

    /// <summary>
    /// 应用程序主模块，用于注入该容器的服务
    /// </summary>
    [DependsOn(typeof(QWpfModule))]
    public class AppModule : QModule
    {
        public override void ConfigureServices(ServiceConfigurationContext context)
        {
            //注入窗口类
            context.ServiceBuilder.RegisterType<MainWindow>().SingleInstance();
            context.ServiceBuilder.RegisterType<JobEditWindow>();
            context.ServiceBuilder.RegisterType<SysCfgWindow>();


            //注入主窗口ViewModel
            context.ServiceBuilder.RegisterType<MainWindowVM>().SingleInstance();
            context.ServiceBuilder.RegisterType<JobEditWindowVM>();
            context.ServiceBuilder.RegisterType<SysCfgWindowVM>();


            context.ServiceBuilder.RegisterType<DirectoryChangedDependJobExecutor>();
            context.ServiceBuilder.RegisterType<TimerDependJobExecutor>();
            context.ServiceBuilder.RegisterType<FileChangedDependJobExecutor>();
            //Format

            //context.ServiceBuilder.RegisterType<Format>().As<IFormat>();
            context.ServiceBuilder.RegisterType<About>().SingleInstance();



        }
    }
}

﻿using GameSave.Infrastructure.Entities;
using Quick;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameSave.Infrastructure.Interfaces
{
    public interface IJobExecutor
    {
        void InitExecutor(JobContext context, SysConfig sysContext, IMessenger msger);

        void Start();

        void Stop();

    }
}

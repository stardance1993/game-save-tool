﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameSave.Infrastructure.Data
{
    public class DefaultConfig
    {
        public static string ConfigDirectory
        {
            get
            {
                return "Configs";
            }
        }

        public static string JobConfigFileName
        {
            get
            {
                return "jobs.json";
            }
        }

        public static string SysConfigFileName
        {
            get
            {
                return "system.json";
            }
        }
        
        public static string GameSaveConfigDirectory
        {
            get
            {
                return "GameSaves";
            }
        }
    }
}

﻿using GameSave.Common.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameSave.Infrastructure.Entities
{
    public class JobContext
    {
        public Job CurrentJob { get; set; }
    }
}

﻿
using Newtonsoft.Json;
using PropertyChanged;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace GameSave.Common.Entities
{
    [AddINotifyPropertyChangedInterface]
    public class Job
    {
        /// <summary>
        /// 任务名
        /// </summary>
        public string JobName { get; set; } 
        /// <summary>
        /// 任务ID
        /// </summary>
        public string JobId { get; set; }
        /// <summary>
        /// 创建日期
        /// </summary>
        public DateTime CreateDate { get; set; }
        /// <summary>
        /// 任务类型(按文件改动触发、定时触发)
        /// </summary>
        public string JobType { get; set; }
        /// <summary>
        /// 运行后自动启动任务
        /// </summary>
        public bool AutoRunJobWhenLaunchApp { get; set; }

        /// <summary>
        /// 最后一次备份的消息
        /// </summary>
        public DateTime? LastExecuteDate { get; set; }

        [JsonIgnore]
        private bool _like;
        /// <summary>
        /// 收藏
        /// </summary>
        public bool Like
        {
            get
            {
                return _like;
            }
            set
            {
                _like = value;
            }
        }

        /// <summary>
        /// 更新时消息
        /// </summary>
        public string UpdateText { get; set; }

        /// <summary>
        /// 备份次数
        /// </summary>
        public int SaveOperationCount { get; set; }

        /// <summary>
        /// 定时备份间隔(分钟)
        /// </summary>
        public int BackupInterval { get; set; }


        /// <summary>
        /// 需要备份的文件夹
        /// </summary>
        public string TargetBackupDirectory { get; set; }

        public string TargetBackupFile { get; set; }


        /// <summary>
        /// 是否运行
        /// </summary>
        [JsonIgnore]
        private bool _isRunning;
        [JsonIgnore]
        public bool IsRunning
        {
            get
            {
                return _isRunning;
            }
            set
            {
                _isRunning = value;
            }
        }

        public int AutoSaveBackupLimit { get; set; }
    }

    
}

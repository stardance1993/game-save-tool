﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameSave.Infrastructure.Entities
{
    public class SysEnum
    {

    }

    public enum JoBType
    {
        单文件备份,
        多文件备份,
        定时备份,
        手动备份
    }

    public enum EKey
    {
        Shift = 16,
        Control = 17,
        Alt = 18,
        A = 65,
        B = 66,
        C = 67,
        D = 68,
        E = 69,
        F = 70,
        G = 71,
        H = 72,
        I = 73,
        J = 74,
        K = 75,
        L = 76,
        M = 77,
        N = 78,
        O = 79,
        P = 80,
        Q = 81,
        R = 82,
        S = 83,
        T = 84,
        U = 85,
        V = 86,
        W = 87,
        X = 88,
        Y = 89,
        Z = 90
    }

    public enum EHotKeyAction
    {
        手动备份,
        载入备份
    }

    public enum Language
    {
        中文简体,
        English
    }
}

﻿
using PropertyChanged;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameSave.Infrastructure.Entities
{
    [AddINotifyPropertyChangedInterface]
    public class HotKeyModel
    {
        public string ActionName { get; set; }


        public bool IsControl { get; set; }

        public bool IsShift { get; set; }

        public bool IsAlt { get; set; }

        public EKey Key { get; set; }

        public string KeyName { get; set; }


        public static Array Keys
        {
            get
            {
                return Enum.GetValues(typeof(EKey));
            }
        }
    }
}

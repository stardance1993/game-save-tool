﻿using PropertyChanged;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace GameSave.Infrastructure.Entities
{
    [AddINotifyPropertyChangedInterface]
    public class SysConfig
    {
        /// <summary>
        /// 备份存档的基目录
        /// </summary>
        public string SaveDirectory { get; set; }
        /// <summary>
        /// 文件变动时，设置一个延时，延时触发时才备份文件
        /// 当短时间内发生新文件变动，将立即取消上个文件变更产生的延时，并设置自己的延时
        /// </summary>
        public int BackupTriggerColdDown { get; set; }


        private string _selfHelpBackupSc;

        public string SelfHelpBackupShortcut
        {
            get { return _selfHelpBackupSc; }
            set { _selfHelpBackupSc = value; }
        }


        private string _loadbackupShortCut;

        public string LoadBackupShortcut
        {
            get { return _loadbackupShortCut; }
            set { _loadbackupShortCut = value; }
        }



        private string _selectedLanguage;

        public string SelectedLanguage
        {
            get { return _selectedLanguage; }
            set { _selectedLanguage = value; }
        }


    }
}

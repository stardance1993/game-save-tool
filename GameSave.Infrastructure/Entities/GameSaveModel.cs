﻿using Newtonsoft.Json;
using PropertyChanged;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace GameSave.Infrastructure.Entities
{
    [AddINotifyPropertyChangedInterface]
    public class GameSaveModel
    {
        /// <summary>
        /// 任务ID
        /// </summary>
        public string JobID { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime CreateDate { get; set; }

        /// <summary>
        /// 描述
        /// </summary>
        private string _desc;

        public string Desc
        {
            get
            {
                return _desc;
            }
            set
            {
                _desc = value;
            }
        }


        /// <summary>
        /// 喜欢
        /// </summary>
        private bool _like;

        public bool Like
        {
            get { return _like; }
            set 
            { 
                _like = value;
            }
        }



        /// <summary>
        /// 存档序号(整数、自增)
        /// </summary>
        public int GameSaveSerialNum { get; set; }

        [JsonIgnore]
        private bool _editStatus;

        public bool EditStatus
        {
            get
            {
                return _editStatus;
            }
            set 
            { 
                if( _editStatus != value ) 
                {
                    _editStatus = value;
                }
            }
        }


        public string BackupRelativePath { get; set; }

        /// <summary>
        /// 是否自动执行的备份
        /// </summary>
        public bool IsAutoSave { get; set; }


    }
}

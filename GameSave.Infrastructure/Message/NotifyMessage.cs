﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameSave.Infrastructure.Message
{
    public class NotifyMessage
    {
        public string Content { get; set; }

        public bool IsAlert { get; set; }


    }
}

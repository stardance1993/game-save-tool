﻿using GameSave.Infrastructure.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameSave.Infrastructure.Message
{
    public class NewBackupCreateMessage
    {
        public GameSaveModel NewBackup { get; set; }
    }
}

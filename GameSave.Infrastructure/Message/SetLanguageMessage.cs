﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameSave.Infrastructure.Message
{
    public  class SetLanguageMessage
    {
        public string CultureName { get; set; }

        public string ResourcePath { get; set; }

    }
}

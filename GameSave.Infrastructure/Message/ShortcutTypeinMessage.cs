﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameSave.Infrastructure.Message
{
    public class ShortcutTypeinMessage
    {
        public string KeyFunction { get; set; }

        public string KeyCombine { get; set; }
    }
}

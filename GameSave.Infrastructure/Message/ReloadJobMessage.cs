﻿using GameSave.Common.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameSave.Infrastructure.Message
{
    public class ReloadJobMessage
    {
        public Job  PrepareUpdateJob { get; set; }

    }
}
